Event_Probability

Event_Importance

Event_Type

KB_ArchivDate

KB_ArchivID

KB_Archivist

KB_ArchivURL

Event_CoordSys

Event_CoordUnit

Event_EndTime

Event_StartTime

Event_Expires

Event_Coord1

Event_Coord2

Event_Coord3

Event_MapURL

Event_MaskURL

Event_PeakTime

Event_C1Error

Event_C2Error

Event_ClippedSpatial

Event_ClippedTemporal

Event_TestFlag

Event_Description

FRM_Contact

FRM_DateRun

FRM_HumanFlag

FRM_Identifier

FRM_Institute

FRM_Name

FRM_ParamSet

FRM_VersionNumber

FRM_URL

FRM_SpecificID

OBS_Observatory

OBS_ChannelID

OBS_Instrument

OBS_MeanWavel

OBS_WavelUnit

OBS_Title

Bound_CCNsteps

Bound_CCStartC1

Bound_CCStartC2

Bound_ChainCode

BoundBox_C1LL

BoundBox_C2LL

BoundBox_C1UR

BoundBox_C2UR

ChainCodeType

RasterScan

RasterScanType

Skel_ChainCode

Skel_Curvature

Skel_Nsteps

Skel_StartC1

Skel_StartC2

AR_McIntoshCls

AR_MtWilsonCls

AR_ZurichCls

AR_PenumbraCls

AR_CompactnessCls

AR_NOAAclass

AR_NOAANum

AR_NumSpots

AR_Polarity

AR_SpotAreaRaw

AR_SpotAreaRawUncert

AR_SpotAreaRawUnit

AR_SpotAreaRepr

AR_SpotAreaReprUncert

AR_SpotAreaReprUnit

SHARP_NOAA_ARS

IntensMin

IntensMax

IntensMean

IntensMedian

IntensVar

IntensSkew

IntensKurt

IntensTotal

IntensUnit

FL_GOESCls

CME_RadialLinVel

CME_RadialLinVelUncert

CME_RadialLinVelMin

CME_RadialLinVelMax

CME_RadialLinVelStddev

CME_RadialLinVelUnit

CME_AngularWidth

CME_AngularWidthUnit

CME_Accel

CME_AccelUncert

CME_AccelUnit

CME_Mass

CME_MassUncert

CME_MassUnit

Area_AtDiskCenter

Area_AtDiskCenterUncert

Area_Raw

Area_Uncert

Area_Unit

Event_Npixels

Event_PixelUnit

FreqMaxRange

FreqMinRange

FreqPeakPower

FreqUnit

IntensMaxAmpl

IntensMinAmpl

OscillNPeriods

OscillNPeriodsUncert

PeakPower

PeakPowerUnit

VelocMaxAmpl

VelocMaxPower

VelocMaxPowerUncert

VelocMinAmpl

VelocUnit

WaveDisplMaxAmpl

WaveDisplMinAmpl

WaveDisplUnit

WavelMaxPower

WavelMaxPowerUncert

WavelMaxRange

WavelMinRange

WavelUnit

EF_PosPeakFluxOnsetRate

EF_NegPeakFluxOnsetRate

EF_OnsetRateUnit

EF_SumPosSignedFlux

EF_SumNegSignedFlux

EF_FluxUnit

EF_AxisOrientation

EF_AxisOrientationUnit

EF_AxisLength

EF_PosEquivRadius

EF_NegEquivRadius

EF_LengthUnit

EF_AspectRatio

EF_ProximityRatio

MaxMagFieldStrength

MaxMagFieldStrengthUnit

Outflow_Length

Outflow_LengthUnit

Outflow_Width

Outflow_WidthUnit

Outflow_Speed

Outflow_TransSpeed

Outflow_SpeedUnit

Outflow_OpeningAngle

OBS_DataPrepURL

FL_PeakFlux

FL_PeakFluxUnit

FL_PeakTemp

FL_PeakTempUnit

FL_PeakEM

FL_PeakEMUnit

FL_EFoldTime

FL_EFoldTimeUnit

FL_Fluence

FL_FluenceUnit

FL_HalphaClass

CD_Area

CD_AreaUncert

CD_AreaUnit

CD_Volume

CD_VolumeUncert

CD_VolumeUnit

CD_Mass

CD_MassUncert

CD_MassUnit

FI_Length

FI_LengthUnit

FI_Tilt

FI_BarbsTot

FI_BarbsR

FI_BarbsL

FI_Chirality

FI_BarbsStartC1

FI_BarbsStartC2

FI_BarbsEndC1

FI_BarbsEndC2

SG_Shape

SG_Chirality

SG_Orientation

SG_AspectRatio

SG_PeakContrast

SG_MeanContrast

OBS_FirstProcessingDate

OBS_LastProcessingDate

OBS_LevelNum

OBS_IncludesNRT

SS_SpinRate

SS_SpinRateUnit

CC_MajorAxis

CC_MinorAxis

CC_AxisUnit

CC_TiltAngleMajorFromRadial

CC_TiltAngleUnit

TO_Shape

UnsignedFlux

MagFluxUnit

MeanInclinationGamma

MeanGradientTotal

MeanGradientVert

MeanGradientHorz

GradientUnit

MeanVertCurrentDensity

CurrentDensityUnit

UnsignedVertCurrent

CurrentUnit

MeanTwistAlpha

TwistUnit

MeanCurrentHelicity

UnsignedCurrentHelicity

AbsNetCurrentHelicity

CurrentHelicityUnit

SAVNCPP

MeanPhotoEnergyDensity

MeanEnergyDensityUnit

TotalPhotoEnergyDensity

TotalEnergyDensityUnit

TotalPhotoEnergy

TotalPhotoEnergyUnit

MeanShearAngle

HighShearAreaPercent

HighShearArea

HighShearAreaUnit

Log_R_Value

GWILL

GWILLUnit

AR_AxisLength

AR_LengthUnit

AR_SumPosSignedFlux

AR_SumNegSignedFlux

AR_NeutralLength

AR_PILCurvature

Search_FRM_Name

Search_Observatory

Search_ChannelID

Search_Instrument

