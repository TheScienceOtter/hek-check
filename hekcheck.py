from sunpy.net import hek
import numpy as np
import pickle
import datetime
from pathlib import Path

hek_properties = []

class NOAA_Object:
    def __init__(self, properties=None, noaa_number=None, start_date=None):
        if properties is None:
            self.properties = {
                "ar_noaanum"        : noaa_number,
                "flares"            : [],
                "start_date"        : start_date,
                "end_date"          : None,
                "spot_count"        : [],
                "sunspot_area"      : [],
                "mean_sunspot_area" : None,
                "hale_class"        : [],
                "mcintosh_class"    : []
            }
        else:
            self.properties = properties

    def __reduce__(self):
        return (self.__class__, (self.properties,))

    def get(self, property):
        return self.properties[property]

    def set(self, property, value):
        self.properties[property] = value

    def append_value(self, property, value):
        if property not in self.properties.keys():
            self.properties[property] = []
        if value is not None:
            self.properties[property].append(value)

    def update_value(self, property, value):
        """determines how a property value should be updated. Because it's not as easy as you'd think"""
        if property == 'event_starttime':
            date = HEKToDatetime(value)
            if date < self.properties[property]:
                self.properties[property] = date
        elif property == 'event_endtime':
            date = HEKToDatetime(value)
            if date > self.properties[property]:
                self.properties[property] = date
        else:
            self.append_value(property, value)



def get_hek_data(hek_client, noaa_database, start_date, end_date):
    start_date = start_date - datetime.timedelta(hours=12)
    end_date = end_date + datetime.timedelta(hours=12)
    print("Searching HEK between {0} and {1}...".format(start_date, end_date))
    AR_result = hek_client.search(hek.attrs.Time(start_date, end_date), hek.attrs.EventType('AR'))
    print("Active regions found: {0}".format(len(AR_result)))
    FL_result = hek_client.search(hek.attrs.Time(start_date, end_date), hek.attrs.EventType('FL'))
    print("Flares found: {0}".format(len(FL_result)))

    # Update AR database
    print("Comparing Active Region databases...")
    new_active_region_count = 0
    for entry in AR_result:
        noaa_num = entry.get('ar_noaanum')
        if noaa_num is None:
            continue
        # If there is already an entry for this noaa number, update it with new information, otherwise make new
        noaa_object = next((x for x in noaa_database if x.get('ar_noaanum') == noaa_num), None)
        if noaa_object is not None:
            for parameter in hek_properties:
                noaa_object.update_value(parameter, entry.get(parameter))
            noaa_object.set('mean_sunspot_area', np.nanmean(noaa_object.get('AR_SpotAreaRaw')))
        else:
            noaa_object = NOAA_Object(noaa_number=noaa_num, start_date=HEKToDatetime(entry.get('event_starttime')))
            new_active_region_count += 1
            noaa_database.append(noaa_object)
            for parameter in hek_properties:
                noaa_object.update_value(parameter, entry.get(parameter))
            noaa_object.set('mean_sunspot_area', np.nanmean(noaa_object.get('AR_SpotAreaRaw')))

    # Check FL result for any flares and associate them with known ARs
    print("Checking flare records...")
    for entry in FL_result:
        noaa_num = entry.get('ar_noaanum')
        if noaa_num is None:
            continue
        if noaa_num < 10000:
            print("NOAA number ({0}) less than 10000? Fixing...".format(noaa_num))
            noaa_num += 10000
        noaa_object = next((x for x in noaa_database if x.get('ar_noaanum') == noaa_num), None)
        if noaa_object is not None:
            noaa_object.append_value('flares', '{0}__{1}'.format(entry.get('fl_goescls'), entry.get('event_starttime')))

    print("{0} new active regions found!".format(new_active_region_count))
    return noaa_database

def search_database(database, client, start_date=None, end_date=None, flares=None):
    # initialise
    if start_date is None:
        start_date = database[0].get('start_date')
    if end_date is None:
        end_date = database[-1].get('start_date')

    # try to find the start and end points in the database. if the dates are out of range, download new data, to fix
    # the gap.
    print("Searching database...")
    start_index = None
    end_index = None
    start_find_result = findDateInDatabase(database, start_date)
    if start_find_result[1] is None:
        start_index = start_find_result[0]
    end_find_result = findDateInDatabase(database, end_date)
    if end_find_result[1] is None:
        end_index = end_find_result[0]
    print("start_index: {0} end_index: {1} ".format(start_find_result, end_find_result))

    # If start_date and end_date are bools
    if start_index is None and end_index is None:
        print("Could not find match in database. Contacting HEK...")
        if start_find_result[1] and end_find_result[1]:
            raise Exception("Impossible state: Start date and end date deltas both positive.")
        elif start_find_result[1] and not end_find_result[1]:
            # Plan 2
            print("Expanding database...")
            database = get_hek_data(client, database, start_date, database[0].get('start_date'))
            start_index = findDateInDatabase(database, start_date)[0]
            end_index = findDateInDatabase(database, end_date)[0]
        elif not start_find_result[1] and end_find_result[1]:
            # plan 4
            print("Expanding database...")
            start_index = len(database)
            database = get_hek_data(client, database, database[-1].get('start_date'), end_date)
            end_index = len(database)
        else:
            raise Exception("Impossible state: Start date and end date deltas both negative.")
    # If start date is a bool but end date isn't
    elif start_index is None and end_index is not None:
        print("Could not find match in database. Contacting HEK...")
        if start_find_result[1]:
            print("Expanding database...")
            database = get_hek_data(client, database, start_date, database[0].get('start_date'))
            start_index = findDateInDatabase(database, start_date)[0]
            end_index = findDateInDatabase(database, end_date)[0]
        else:
            raise Exception("Impossible state: Start date delta cannot be negative if an end date exists.")
    # If start date is not a bool but end date is
    elif start_find_result[1] is not None and end_find_result[1] is None:
        print("Could not find match in database. Contacting HEK...")
        if end_find_result[1]:
            print("Expanding database...")
            database = get_hek_data(client, database, database[-1].get('start_date'), end_date)
            end_index = findDateInDatabase(database, end_date)[0]

    # Now deal with request
    print("Retrieving data...")
    print("start_index: {0} end_index: {1}".format(start_index, end_index))
    database_slice = database[start_index:end_index]

    if flares is None:
        return database_slice
    elif flares == True:
        return [entry for entry in database_slice if len(entry.get('flares')) > 0]
    elif flares == False:
        return [entry for entry in database_slice if len(entry.get('flares')) == 0]
    else:
        return None


def findDateInDatabase(database, date):
    """Find the index of the entry in the database that has the specified datetime. Second element in returned tuple
    is a bool to say if the list elements are before or after the specified time. if no match is found, the bool will
    be True if all elements in the list occurred after the checked date, and false if they all happened before. It will
    be none if a match is found."""
    start_datetime = database[0].get('start_date') if isinstance(database[0].get('start_date'), datetime.datetime) else HEKToDatetime(database[0].get('start_date'))
    last_delta_time = (start_datetime - date).total_seconds()
    for i in range(1, len(database)):
        start_datetime = database[i].get('start_date') if isinstance(database[0].get('start_date'), datetime.datetime) else HEKToDatetime(database[0].get('start_date'))
        delta_time = (start_datetime - date).total_seconds()
        if delta_time * last_delta_time < 0:
            return (i, None)
        last_delta_time = delta_time
    return (i, last_delta_time > 0)


def HEKToDatetime(hek_time):
    """Converts the string representation of time received from the HEK data into a python datetime"""
    return datetime.datetime.strptime(hek_time, "%Y-%m-%dT%H:%M:%S")


def getHEKProperties(properties_path):
    """Reads the hek_properties file to get the list of keys for the NOAA objects"""
    path = Path(properties_path)
    print("Loading HEK parameters...")
    try:
        with open(path) as f:
            hek_params = f.readlines()
        hek_params = [e.strip() for e in hek_params]
        hek_params = [e for e in hek_params if e != '']
        print("Loaded {0} parameters".format(len(hek_params)))
        return  hek_params
    except FileNotFoundError:
        print("HEK properties file not found, unable to load")
        return None


def saveDatabase(path, database):
    print("Saving database...")
    savepath = Path(path) / 'noaa_database.dat'
    with savepath.open('wb') as f:
        pickle.dump(database, f)
    return True


def loadDatabase(path):
    print("Attempting to load local database...")
    checkPath(path)
    loadpath = Path(path) / 'noaa_database.dat'
    try:
        if loadpath.stat().st_size > 0:
            with loadpath.open('rb') as f:
                database = pickle.load(f)
            return database
        else:
            return None
    except FileNotFoundError:
        print("Database not found, unable to load")
        return None

def checkPath(_path):
    '''
    Checks to see if _path exists and returns the path as a string. If the path doesn't exist, it is made.
    '''
    if isinstance(_path, str):
        pyPath = Path(_path)
    elif isinstance(_path, Path):
        pyPath = _path
    else:
        raise ValueError("_path must be a string or pathlib Path.")

    if not pyPath.is_dir():
        pyPath.mkdir(parents=True)
        print("DEBUG: Making new path '{0}'...".format(str(pyPath.resolve())))

    return pyPath


def print_results(results):
    print("{0:<15}|{1}|{2:>9}|{3:>12}|{4:>12}".format("NOAA Number", "Start Date","Flares","Av. Area", "Av. Spots"))
    if len(results) == 0:
        print("No matches found.")
    else:
        for entry in results:
            print("{0:<15}|{1}|{2:>9}|{3:>12}|{4:>12}".format(entry.get('ar_noaanum'),
                                                       entry.get('start_date'),
                                                       len(entry.get('flares')),
                                                       entry.get('mean_sunspot_area'),
                                                       np.nanmean(entry.get('AR_NumSpots'))))


if __name__ == "__main__":
    # Initialise
    client = hek.HEKClient()
    saveload_path = '/home/richard/work/'
    noaa_database = loadDatabase(saveload_path)
    if noaa_database is None:
        noaa_database = []
    hek_properties = getHEKProperties('/home/richard/work/hek_parameters.txt')

    #TODO: Get user input from args
    start_date = HEKToDatetime('2014-09-01T00:00:00')
    end_date = HEKToDatetime('2014-09-30T00:00:00')
    if len(noaa_database) == 0:
        noaa_database = get_hek_data(client, noaa_database, start_date, end_date)
    results = search_database(noaa_database, client, start_date, end_date, flares=False)
    saveDatabase(saveload_path, noaa_database)

    print_results(results)

